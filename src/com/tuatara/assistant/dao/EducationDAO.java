package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tuatara.assistant.domain.Education;
import com.tuatara.assistant.mappers.EducationMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class EducationDAO implements EducationMapper{

	@Override
	public void insertEducation(Integer candidateId, Education education) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			EducationMapper educationMapper = sqlSession.getMapper(EducationMapper.class);
			educationMapper.insertEducation(candidateId, education);
			sqlSession.commit();
		}
	}

	@Override
	public List<Education> getEducation(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			EducationMapper educationMapper = sqlSession.getMapper(EducationMapper.class);
			return educationMapper.getEducation(candidateId);
		}
	}

	@Override
	public void deleteEducation(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			EducationMapper educationMapper = sqlSession.getMapper(EducationMapper.class);
			educationMapper.deleteEducation(candidateId);
			sqlSession.commit();
		}
	}

}
