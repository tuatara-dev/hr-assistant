package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.tuatara.assistant.domain.User;
import com.tuatara.assistant.mappers.UserMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class UserDAO implements UserMapper {

	@Override
	public void insertUser(User user) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			
			userMapper.insertUser(user);
			sqlSession.commit();
		}
	}
	
	@Override
	public User getUserById(Integer userId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			User user = userMapper.getUserById(userId);

			return user;
		}
	}

	@Override
	public User getUserByEmail(String email){
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			User user = userMapper.getUserByEmail(email);

			return user;
		}
	}
	
	@Override
	public List<User> getAllUsers() {

		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			List<User> users = userMapper.getAllUsers();

			return users;
		}
	}

	@Override
	public void updateUser(User user) {

		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			
			userMapper.updateUser(user);
			sqlSession.commit();
		}
	}

}
