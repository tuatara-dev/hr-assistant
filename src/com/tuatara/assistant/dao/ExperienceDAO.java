package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tuatara.assistant.domain.Experience;
import com.tuatara.assistant.mappers.ExperienceMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class ExperienceDAO implements ExperienceMapper{

	@Override
	public void insertExperience(Integer candidateId, Experience experience) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			ExperienceMapper experienceMapper = sqlSession.getMapper(ExperienceMapper.class);
			experienceMapper.insertExperience(candidateId, experience);
			sqlSession.commit();
		}
	}

	@Override
	public List<Experience> getExperience(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			ExperienceMapper experienceMapper = sqlSession.getMapper(ExperienceMapper.class);
			return experienceMapper.getExperience(candidateId);
		}
	}

	@Override
	public void deleteExperience(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			ExperienceMapper experienceMapper = sqlSession.getMapper(ExperienceMapper.class);
			experienceMapper.deleteExperience(candidateId);
			sqlSession.commit();
		}
	}


}
