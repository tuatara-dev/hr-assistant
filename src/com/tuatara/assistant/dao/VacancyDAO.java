package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tuatara.assistant.domain.Vacancy;
import com.tuatara.assistant.domain.VacancyProfessionalSkill;
import com.tuatara.assistant.mappers.VacancyMapper;
import com.tuatara.assistant.mappers.VacancyProfessionalSkillMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class VacancyDAO implements VacancyMapper{

	@Override
	public void insertVacancy(Integer userId, Vacancy vacancy) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyMapper vacancyMapper = sqlSession.getMapper(VacancyMapper.class);
			vacancyMapper.insertVacancy(userId, vacancy);
			insertVacancyTrigger(vacancy, sqlSession);
			sqlSession.commit();
		}
	}

	private void insertVacancyTrigger(Vacancy vacancy, SqlSession sqlSession){
		for(VacancyProfessionalSkill profSkill : vacancy.getProfesionalSkills()){
			sqlSession.getMapper(VacancyProfessionalSkillMapper.class).insertProfessionalSkill(vacancy.getVacancyId(), profSkill);
		}
	}
	
	@Override
	public Vacancy getVacancyById(Integer vacancyId) {
		Vacancy vacancy = null;
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyMapper vacancyMapper = sqlSession.getMapper(VacancyMapper.class);
			vacancy = vacancyMapper.getVacancyById(vacancyId);
			selectVacancyTrigger(vacancy, sqlSession);
		}
		return vacancy;
	}
	
	@Override
	public Vacancy getVacancyByName(String vacancyName){
		Vacancy vacancy = null;
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyMapper vacancyMapper = sqlSession.getMapper(VacancyMapper.class);
			vacancy = vacancyMapper.getVacancyByName(vacancyName);
			selectVacancyTrigger(vacancy, sqlSession);
		}
		return vacancy;
	}
	
	@Override
	public List<Vacancy> getVacanciesByUserId(Integer userId) {
		List<Vacancy> vacancies = null;
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyMapper vacancyMapper = sqlSession.getMapper(VacancyMapper.class);
			vacancies = vacancyMapper.getVacanciesByUserId(userId);
			
			for(Vacancy vacancy : vacancies){
				selectVacancyTrigger(vacancy, sqlSession);
			}
			
		}
		return vacancies;
	}
	
	private void selectVacancyTrigger(Vacancy vacancy, SqlSession sqlSession){
		VacancyProfessionalSkillDAO profSkillsDAO = new VacancyProfessionalSkillDAO();
		vacancy.setProfesionalSkills(profSkillsDAO.getProfessionalSkillsByVacancyId(vacancy.getVacancyId()));
		CandidateDAO candidateDAO = new CandidateDAO();
		vacancy.setCandidates(candidateDAO.getCandidatesByVacancyId(vacancy.getVacancyId()));
	}
	
	@Override
	public void updateVacancy(Vacancy vacancy) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyMapper vacancyMapper = sqlSession.getMapper(VacancyMapper.class);
			vacancyMapper.updateVacancy(vacancy);
			updateVacancyTrigger(vacancy, sqlSession);
			sqlSession.commit();
		}
	}

	private void updateVacancyTrigger(Vacancy vacancy, SqlSession sqlSession){
		VacancyProfessionalSkillMapper profSkills = sqlSession.getMapper(VacancyProfessionalSkillMapper.class);
		profSkills.deleteProfessionalSkillByVacancyId(vacancy.getVacancyId());
		for(VacancyProfessionalSkill ps : vacancy.getProfesionalSkills()){
			profSkills.insertProfessionalSkill(vacancy.getVacancyId(), ps);
		}
	}
	
	@Override
	public void deleteVacancy(Integer vacancyId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyMapper vacancyMapper = sqlSession.getMapper(VacancyMapper.class);
			vacancyMapper.deleteVacancy(vacancyId);
			deleteVacancyTrigger(vacancyId);
			sqlSession.commit();
		}
	}
	
	private void deleteVacancyTrigger(Integer vacancyId){
		VacancyProfessionalSkillDAO profSkillsDAO = new VacancyProfessionalSkillDAO();
		profSkillsDAO.deleteProfessionalSkillByVacancyId(vacancyId);
		CandidateDAO candidateDAO = new CandidateDAO();
		candidateDAO.deleteCandidatesByVacancyId(vacancyId);
	}
}
