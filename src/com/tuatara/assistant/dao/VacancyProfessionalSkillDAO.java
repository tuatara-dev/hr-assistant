package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tuatara.assistant.domain.VacancyProfessionalSkill;
import com.tuatara.assistant.mappers.VacancyProfessionalSkillMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class VacancyProfessionalSkillDAO implements VacancyProfessionalSkillMapper{

	@Override
	public void insertProfessionalSkill(Integer vacancyId,
			VacancyProfessionalSkill vacancyProfSkill) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyProfessionalSkillMapper professionalSkillMapper = sqlSession.getMapper(VacancyProfessionalSkillMapper.class);
			professionalSkillMapper.insertProfessionalSkill(vacancyId, vacancyProfSkill);
			sqlSession.commit();
		}
	}

	@Override
	public List<VacancyProfessionalSkill> getProfessionalSkillsByVacancyId(
			Integer vacancyId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyProfessionalSkillMapper professionalSkillMapper = sqlSession.getMapper(VacancyProfessionalSkillMapper.class);
			return professionalSkillMapper.getProfessionalSkillsByVacancyId(vacancyId);
		}
	}

	@Override
	public void deleteProfessionalSkillByVacancyId(Integer vacancyId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			VacancyProfessionalSkillMapper professionalSkillMapper = sqlSession.getMapper(VacancyProfessionalSkillMapper.class);
			professionalSkillMapper.deleteProfessionalSkillByVacancyId(vacancyId);
			sqlSession.commit();
		}
	}


}
