package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tuatara.assistant.domain.Candidate;
import com.tuatara.assistant.domain.Education;
import com.tuatara.assistant.domain.Experience;
import com.tuatara.assistant.domain.LanguageSkill;
import com.tuatara.assistant.mappers.CandidateMapper;
import com.tuatara.assistant.mappers.EducationMapper;
import com.tuatara.assistant.mappers.ExperienceMapper;
import com.tuatara.assistant.mappers.LanguageSkillMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class CandidateDAO implements CandidateMapper{

	@Override
	public void insertCandidate(Integer vacancyId, Candidate candidate) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CandidateMapper candidateMapper = sqlSession.getMapper(CandidateMapper.class);
			candidateMapper.insertCandidate(vacancyId, candidate);
			insertCandidateTrigger(candidate, sqlSession);
			sqlSession.commit();
		}
		
	}

	private void insertCandidateTrigger(Candidate candidate, SqlSession sqlSession){
		if(candidate.getEducation() != null){
			for(Education edu : candidate.getEducation()){
				sqlSession.getMapper(EducationMapper.class).insertEducation(candidate.getCandidateId(), edu);
			}
		}
		if(candidate.getExperience() != null){
			for(Experience exp : candidate.getExperience()){
				sqlSession.getMapper(ExperienceMapper.class).insertExperience(candidate.getCandidateId(), exp);
			}
		}
		if(candidate.getLanguageSkills() != null){
			for(LanguageSkill ls : candidate.getLanguageSkills()){
				sqlSession.getMapper(LanguageSkillMapper.class).insertLanguageSkill(candidate.getCandidateId(), ls);
			}
		}
	}
	
	@Override
	public List<Candidate> getCandidatesByVacancyId(Integer vacancyId) {
		List<Candidate> candidates;
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CandidateMapper candidateMapper = sqlSession.getMapper(CandidateMapper.class);
			candidates = candidateMapper.getCandidatesByVacancyId(vacancyId);
			
			for(Candidate candidate : candidates){
				selectCandidateTrigger(candidate, sqlSession);
			}
			
		}
		
		return candidates;
	}
	
	@Override
	public List<Candidate> getCandidatesByVacancyName(String vacancyName){
		List<Candidate> candidates;
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CandidateMapper candidateMapper = sqlSession.getMapper(CandidateMapper.class);
			candidates = candidateMapper.getCandidatesByVacancyName(vacancyName);
			
			for(Candidate candidate : candidates){
				selectCandidateTrigger(candidate, sqlSession);
			}
		}
		
		return candidates;
	}
	
	@Override
	public Candidate getCandidateById(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CandidateMapper candidateMapper = sqlSession.getMapper(CandidateMapper.class);
			
			Candidate candidate = candidateMapper.getCandidateById(candidateId);
			selectCandidateTrigger(candidate, sqlSession);
			
			return candidate;
		}
	}
	
	@Override
	public Candidate getCandidateByEmail(String email){
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CandidateMapper candidateMapper = sqlSession.getMapper(CandidateMapper.class);
			
			Candidate candidate = candidateMapper.getCandidateByEmail(email);
			selectCandidateTrigger(candidate, sqlSession);
			
			return candidate;
		}
	}
	
	private void selectCandidateTrigger(Candidate candidate, SqlSession sqlSession){
		if (candidate != null) {
			candidate.setLanguageSkills(sqlSession.getMapper(LanguageSkillMapper.class).getLanguageSkills(candidate.getCandidateId()));
			candidate.setExperience(sqlSession.getMapper(ExperienceMapper.class).getExperience(candidate.getCandidateId()));
			candidate.setEducation(sqlSession.getMapper(EducationMapper.class).getEducation(candidate.getCandidateId()));
		}
	}
	
	@Override
	public void deleteCandidateById(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			CandidateMapper candidateMapper = sqlSession.getMapper(CandidateMapper.class);
			
			deleteCandidateTrigger(candidateId, sqlSession);
			candidateMapper.deleteCandidateById(candidateId);
			
			sqlSession.commit();
		}
	}
	
	private void deleteCandidateTrigger(Integer candidateId, SqlSession sqlSession){
		sqlSession.getMapper(EducationMapper.class).deleteEducation(candidateId);
		sqlSession.getMapper(ExperienceMapper.class).deleteExperience(candidateId);
		sqlSession.getMapper(LanguageSkillMapper.class).deleteLanguageSkills(candidateId);
	}

	public void deleteCandidatesByVacancyId(Integer vacancyId) {
		for(Candidate candidate : getCandidatesByVacancyId(vacancyId)){
			deleteCandidateById(candidate.getCandidateId());
		}
	}

}
