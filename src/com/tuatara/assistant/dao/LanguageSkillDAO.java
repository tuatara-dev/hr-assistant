package com.tuatara.assistant.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tuatara.assistant.domain.LanguageSkill;
import com.tuatara.assistant.mappers.LanguageSkillMapper;
import com.tuatara.assistant.service.MyBatisUtil;

public class LanguageSkillDAO implements LanguageSkillMapper{

	@Override
	public void insertLanguageSkill(Integer candidateId, LanguageSkill ls) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LanguageSkillMapper languageMapper = sqlSession.getMapper(LanguageSkillMapper.class);
			languageMapper.insertLanguageSkill(candidateId, ls);
			sqlSession.commit();
		}
	}

	@Override
	public List<LanguageSkill> getLanguageSkills(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LanguageSkillMapper languageMapper = sqlSession.getMapper(LanguageSkillMapper.class);
			return languageMapper.getLanguageSkills(candidateId);
		}
	}

	@Override
	public void deleteLanguageSkills(Integer candidateId) {
		try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()){
			LanguageSkillMapper languageMapper = sqlSession.getMapper(LanguageSkillMapper.class);
			languageMapper.deleteLanguageSkills(candidateId);
			sqlSession.commit();
		}
	}

}
