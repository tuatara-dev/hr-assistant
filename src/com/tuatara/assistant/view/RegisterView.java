package com.tuatara.assistant.view;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.tuatara.assistant.AssistantUI;
import com.tuatara.assistant.presenter.RegisterPresenter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.CustomComponent;

public class RegisterView extends CustomComponent implements View {

	private static final long serialVersionUID = 1L;

	public static final String NAME	 = "register";
	
	private RegisterPresenter presenter;
	
	private TextField 	loginField;
	private TextField 	nameField;
    private PasswordField passwordField;
	private PasswordField repeatedField;
    private TextField imapHostField;
    private TextField imapPortField;
    private TextField smtpHostField;
    private TextField smtpPortField;
    
    private List<AbstractTextField> fields = 
    		(List<AbstractTextField>) Arrays.asList(new AbstractTextField[]{
    				loginField,
    				nameField,
    				passwordField,
    				repeatedField,
    				imapHostField,
    				imapPortField,
    				smtpHostField,
    				smtpPortField});
    
    private Button registerButton;
    private Button cancelButton;
    
    public RegisterView(){
    	
    	setSizeFull();
    	presenter = new RegisterPresenter(this);
    	generateUI();
    	
    }
    
    private void generateUI(){
    	
    	// Create the name input field
    	nameField = new TextField("Name:");
    	nameField.setWidth("300px");
    	nameField.setRequired(false);
    	nameField.setValue("");
    	nameField.setNullRepresentation("");
    	
    	// Create the login input field
    	loginField = new TextField("Email:");
    	loginField.setWidth("300px");
    	loginField.setRequired(true);
		loginField.setInputPrompt("Your email (eg. qwerty@email.com)");
		loginField.addValidator(new EmailValidator(
                "Username must be an email address"));
		loginField.setInvalidAllowed(false);
        
        // Create the password input field
		passwordField = new PasswordField("Password:");
		passwordField.setWidth("300px");
		passwordField.addValidator(new StringLengthValidator(
        		"Password length must be between 8 and 100 characters", 8, 100, false));
		passwordField.setRequired(true);
		
		// Check the password input field
		repeatedField = new PasswordField("Password:");
		repeatedField.setWidth("300px");
		repeatedField.addValidator(new StringLengthValidator(
        		"Password length must be between 8 and 100 characters", 8, 100, false));
		repeatedField.setRequired(true);
		
		// Create the IMAP host input field
		imapHostField = new TextField("IMAP Host:");
		imapHostField.setWidth("140px");
		imapHostField.setRequired(true);
		imapHostField.addValidator(new RegexpValidator("^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$", 
        		"The IMAP host is not valid"));
    	
    	// Create the IMAP port input field
		imapPortField = new TextField("IMAP Port:");
		imapPortField.setWidth("140px");
		imapPortField.setRequired(true);
		imapPortField.addValidator(new IntegerRangeValidator("The IMAP port is not valid", 1, 65535));
    	
    	HorizontalLayout imapFields = new HorizontalLayout(imapHostField, imapPortField);
    	imapFields.setSpacing(true);
    	
    	// Create the SMTP host input field
    	smtpHostField = new TextField("SMTP Host:");
    	smtpHostField.setWidth("140px");
    	smtpHostField.setRequired(true);
    	smtpHostField.addValidator(new RegexpValidator("^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$", 
        		"The SMTP host is not valid"));
    	
    	// Create the SMTP port input field
    	smtpPortField = new TextField("SMTP Port:");
    	smtpPortField.setWidth("140px");
    	smtpPortField.setRequired(true);
    	smtpPortField.addValidator(new IntegerRangeValidator("The SMTP port is not valid", 1, 65535));
    	
    	HorizontalLayout pop3Fields = new HorizontalLayout(smtpHostField, smtpPortField);
    	pop3Fields.setSpacing(true);
    	
    	// Create register button
    	registerButton = new Button("Register");
    	registerButton.addClickListener(new ClickListener(){
        	
        	@Override
			public void buttonClick(ClickEvent event) {
        		
        		presenter.doRegister();
			}
        });
        
        // Create cancel button
    	cancelButton = new Button("Cancel");
    	cancelButton.setStyleName(BaseTheme.BUTTON_LINK);
    	cancelButton.addClickListener(new ClickListener(){
        	
        	@Override
			public void buttonClick(ClickEvent event) {
        		
        		AssistantUI.getCurrent().getNavigator().navigateTo(LoginView.NAME);
			}
        });
        
        HorizontalLayout buttons = new HorizontalLayout(registerButton, cancelButton);
        buttons.setSpacing(true);
        buttons.setMargin(new MarginInfo(true, true, true, false));
        
        // Add both to a panel
        VerticalLayout fields = new VerticalLayout(nameField, loginField, passwordField, 
        		repeatedField, imapFields, pop3Fields, buttons);
        fields.setSpacing(true);
        fields.setMargin(new MarginInfo(true, true, true, true));
        Iterator<Component> it = fields.iterator();
        while(it.hasNext()){
        	fields.setComponentAlignment(it.next(), Alignment.MIDDLE_CENTER);
        }
        
        // The view root panel
        Panel panel = new Panel();
        panel.setContent(fields);
        panel.setSizeFull();

        setCompositionRoot(panel);
    }

    public RegisterPresenter getPresenter() {
		return presenter;
	}

	public TextField getLoginField() {
		return loginField;
	}

	public TextField getNameField() {
		return nameField;
	}

	public PasswordField getPasswordField() {
		return passwordField;
	}

	public PasswordField getRepeatedField() {
		return repeatedField;
	}

	public TextField getImapHostField() {
		return imapHostField;
	}

	public TextField getImapPortField() {
		return imapPortField;
	}

	public TextField getSmtpHostField() {
		return smtpHostField;
	}

	public TextField getSmtpPortField() {
		return smtpPortField;
	}
    
	public List<AbstractTextField> getFields(){
		return fields;
	}
	
	@Override
	public void enter(ViewChangeEvent event) {

		nameField.focus();
	}

}
