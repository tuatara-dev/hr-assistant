package com.tuatara.assistant.view;

import com.tuatara.assistant.presenter.LoginPresenter;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Reindeer;

public class LoginView extends CustomComponent implements View{

	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "login";
	
	private final LoginPresenter presenter;
	
	private TextField 		loginField;
	private PasswordField 	passwordField;
	private Button 			loginButton;
	private Button 			registerButton;
	
	public LoginView(){
		presenter = new LoginPresenter(this);
		setSizeFull();
		generateUI();
	}
	
	private void generateUI(){
		
		// Create the login input field
		loginField = new TextField("Email:");
		loginField.setWidth("300px");
		loginField.setInputPrompt("Your email (eg. qwerty@email.com)");
		loginField.addValidator(new EmailValidator(
                "Username must be an email address"));
        
        // Create the password input field
		passwordField = new PasswordField("Password:");
		passwordField.setWidth("300px");
        passwordField.addValidator(new StringLengthValidator(
        		"Password must be at least 4 characters long", 4, Integer.MAX_VALUE, false));
        
        // Create login button
        loginButton = new Button("Login");
        loginButton.addClickListener(new ClickListener(){	
        	@Override
			public void buttonClick(ClickEvent event) {
        		presenter.doLogin(loginField, passwordField);
			}
        });
        
        // Create register button
        registerButton = new Button("Register");
        registerButton.setStyleName(BaseTheme.BUTTON_LINK);
        registerButton.addClickListener(new ClickListener(){
        	@Override
			public void buttonClick(ClickEvent event) {
        		// Go to the register view
        		presenter.goRegister();
			}
        });
        
        HorizontalLayout buttons = new HorizontalLayout(loginButton, registerButton);
        buttons.setSpacing(true);
        buttons.setMargin(new MarginInfo(true, true, true, false));
        buttons.setSizeUndefined();
        
        // Add both to a panel
        VerticalLayout fields = new VerticalLayout(loginField, passwordField, buttons);
        fields.setCaption("Please login to access the application or sing up.");
        fields.setSpacing(true);
        fields.setMargin(new MarginInfo(true, true, true, false));
        fields.setSizeUndefined();
        
        // Create root layout
        VerticalLayout root = new VerticalLayout(fields);
        root.setSizeFull();
        root.setComponentAlignment(fields, Alignment.MIDDLE_CENTER);
        root.setStyleName(Reindeer.LAYOUT_BLUE);
        
        setCompositionRoot(root);
	}
	
	public void enter(ViewChangeEvent event) {
	    // focus the login field when hr arrives to the login view
		loginField.focus();
	}

	 public void enableFields(boolean e){
		loginField.setEnabled(e);
		passwordField.setEnabled(e);
		loginButton.setEnabled(e);
		registerButton.setEnabled(e);
	}

}
