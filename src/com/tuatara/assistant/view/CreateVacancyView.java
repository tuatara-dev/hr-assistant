package com.tuatara.assistant.view;

import java.util.Iterator;

import com.tuatara.assistant.presenter.CreateVacancyPresenter;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.slider.SliderOrientation;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Reindeer;
import com.vaadin.ui.CustomComponent;

public class CreateVacancyView extends CustomComponent implements View {
	
	private static final long serialVersionUID = 1L;

	public static final String NAME	 = "newvacancy";
	
	private CreateVacancyPresenter presenter;
	
	private TextField 	nameField;
	private TextArea 	autoResponseArea;
	private TextField 	regionField;
	private ComboBox 	professionalSkillsComboBox;
	private NativeSelect 	professionalSkillsPriority;
	private Slider 		experienceSlider;
	private NativeSelect 	experiencePriority;
	private ListSelect 	languageSkillsList;
	private NativeSelect 	languageSkillsPriority;
	private CheckBox 	educationCheckBox;
	private NativeSelect 	educationPriority;
	private CheckBox 	coursesCheckBox;
	private NativeSelect 	coursesPriority;
	private Button 		saveButton;
    private Button 		cancelButton;
	
	public CreateVacancyView(){
		setSizeFull();
		presenter = new CreateVacancyPresenter(this);
		generateUI();
	}
	

	private void generateUI(){
		
		// Create the vacancy name input field
    	nameField = new TextField("Name:");
    	nameField.setWidth("410px");
    	nameField.setRequired(true);
    	nameField.setValue("");
    	nameField.setNullRepresentation("");
		
    	// Create area for auto response email message
    	autoResponseArea = new TextArea("auto response:");
    	autoResponseArea.setWidth("410px");
    	autoResponseArea.setValue("");
    	autoResponseArea.setNullRepresentation("");
    	
    	// Create the region input field
    	regionField = new TextField("Region:");
    	regionField.setWidth("300px");
    	regionField.setRequired(false);
    	regionField.setValue("");
    	regionField.setNullRepresentation("");
    	
    	// Create combobox for input professional skills
    	professionalSkillsComboBox = new ComboBox("Professional Skills:");
    	professionalSkillsComboBox.setWidth("200px");
    	professionalSkillsComboBox.setTextInputAllowed(true);
    	professionalSkillsComboBox.setNewItemsAllowed(true);
    	professionalSkillsComboBox.setNullSelectionAllowed(false);
    	
    	// Create combobox for input professional skills priority
    	professionalSkillsPriority = new NativeSelect("Professional Skills Priority:");
    	professionalSkillsPriority.setWidth("200px");
    	addPriorityValues(professionalSkillsPriority);
    	
    	HorizontalLayout professionalSkillsFields = new HorizontalLayout(professionalSkillsComboBox, 
    			professionalSkillsPriority);
    	professionalSkillsFields.setSpacing(true);
    	professionalSkillsFields.setMargin(new MarginInfo(true, true, true, true));
    	professionalSkillsFields.setSizeUndefined();
    	
    	// Create slider for input experience value
    	experienceSlider = new Slider("Experience:");
    	experienceSlider.setMin(0);
    	experienceSlider.setMax(10);
    	experienceSlider.setWidth("200px");
    	experienceSlider.setOrientation(SliderOrientation.HORIZONTAL);
    	
    	// Create combobox for input experience priority
    	experiencePriority = new NativeSelect("Experience Priority:");
    	experiencePriority.setWidth("200px");
    	addPriorityValues(experiencePriority);
    	
    	HorizontalLayout experienceFields = new HorizontalLayout(experienceSlider, experiencePriority);
    	experienceFields.setSpacing(true);
    	experienceFields.setMargin(new MarginInfo(true, true, true, true));
    	experienceFields.setSizeUndefined();
    	
    	educationCheckBox = new CheckBox("Consider education");
    	educationCheckBox.setWidth("200px");
    	
    	// Create combobox for input courses priority
    	educationPriority = new NativeSelect("Education Priority:");
    	educationPriority.setWidth("200px");
    	addPriorityValues(educationPriority);
    	
    	VerticalLayout EducationFields = new VerticalLayout(educationCheckBox, educationPriority);
    	EducationFields.setSizeUndefined();
    	
    	coursesCheckBox = new CheckBox("Consider sertificats");
    	coursesCheckBox.setWidth("200px");
    	
    	// Create combobox for input courses priority
    	coursesPriority = new NativeSelect("Courses Priority:");
    	coursesPriority.setWidth("200px");
    	addPriorityValues(coursesPriority);
    	
    	VerticalLayout CourseFields = new VerticalLayout(coursesCheckBox, coursesPriority);
    	CourseFields.setSizeUndefined();
    	
    	HorizontalLayout EducationAndCourseFields = new HorizontalLayout(EducationFields, CourseFields);
    	EducationAndCourseFields.setSpacing(true);
    	EducationAndCourseFields.setMargin(new MarginInfo(true, true, true, true));
    	EducationAndCourseFields.setSizeUndefined();
    	
    	// Create list for input language skills
    	languageSkillsList = new ListSelect();
    	
    	languageSkillsList.setMultiSelect(false);
    	languageSkillsList.setNullSelectionAllowed(false);
    	languageSkillsList.addItem("English");
    	languageSkillsList.addItem("Spanish");
    	languageSkillsList.addItem("Italian");
    	languageSkillsList.addItem("German");
    	languageSkillsList.addItem("French");
    	languageSkillsList.addItem("Russian");
    	languageSkillsList.addItem("Ukrainian");
    	languageSkillsList.setWidth("200px");
    	
    	// Create combobox for input language skills priority
    	languageSkillsPriority = new NativeSelect("Language Skills Priority:");
    	languageSkillsPriority.setWidth("200px");
    	addPriorityValues(languageSkillsPriority);
    	
    	// Create register button
    	saveButton = new Button("Save");
    	saveButton.addClickListener(new ClickListener(){
        	
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
        		
        		presenter.createVacancy();
			}
        });
        
        // Create cancel button
    	cancelButton = new Button("Cancel");
    	cancelButton.setStyleName(BaseTheme.BUTTON_LINK);
    	cancelButton.addClickListener(new ClickListener(){
        	
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
        		
        		presenter.doCancel();
			}
        });
        
        HorizontalLayout buttons = new HorizontalLayout(saveButton, cancelButton);
        buttons.setSpacing(true);
        buttons.setMargin(new MarginInfo(true, true, true, true));
        buttons.setSizeUndefined();
        
        // Add both to a panel
        VerticalLayout fields = new VerticalLayout(nameField, autoResponseArea, regionField,
        		professionalSkillsFields, experienceFields, EducationAndCourseFields, 
        		languageSkillsList, languageSkillsPriority, buttons);
        fields.setSpacing(true);
        fields.setMargin(new MarginInfo(true, true, true, true));

        Iterator<Component> it = fields.iterator();
        while(it.hasNext()){
        	fields.setComponentAlignment(it.next(), Alignment.MIDDLE_CENTER);
        }
        
        Panel panel = new Panel();		
		panel.setContent(fields);
        panel.setSizeFull();
        panel.setStyleName(Reindeer.LAYOUT_BLUE);
        setCompositionRoot(panel);
	}

	
	public TextField getNameField() {
		return nameField;
	}


	public TextArea getAutoResponseArea() {
		return autoResponseArea;
	}


	public TextField getRegionField() {
		return regionField;
	}


	public ComboBox getProfessionalSkillsComboBox() {
		return professionalSkillsComboBox;
	}


	public NativeSelect getProfessionalSkillsPriority() {
		return professionalSkillsPriority;
	}


	public Slider getExperienceSlider() {
		return experienceSlider;
	}


	public NativeSelect getExperiencePriority() {
		return experiencePriority;
	}


	public ListSelect getLanguageSkillsList() {
		return languageSkillsList;
	}


	public NativeSelect getLanguageSkillsPriority() {
		return languageSkillsPriority;
	}


	public CheckBox getEducationCheckBox() {
		return educationCheckBox;
	}


	public NativeSelect getEducationPriority() {
		return educationPriority;
	}


	public CheckBox getCoursesCheckBox() {
		return coursesCheckBox;
	}


	public NativeSelect getCoursesPriority() {
		return coursesPriority;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		nameField.focus();
	}
	
	private void addPriorityValues(NativeSelect component){
		component.setNullSelectionItemId(component.addItem("Not Important"));
		component.addItem("Low");
		component.addItem("Middle");
		component.addItem("High");
		component.setRequired(true);
		component.setNullSelectionAllowed(false);
	}

}
