package com.tuatara.assistant.view;

import java.util.Iterator;

import com.tuatara.assistant.presenter.CandidatesPresenter;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class CandidatesView extends CustomComponent implements View{

	private static final long serialVersionUID = 1L;

	public static final String NAME 	 = "candidates";
	
	CandidatesPresenter presenter;
	
	private Table 	candidatesTable;
	private Button 	newVacancyButton;
	private Button 	logOutButton;
	
	public CandidatesView(){
		
		setSizeFull();
		presenter = new CandidatesPresenter(this);
		generateUI();
	}
	
	private void generateUI(){
		
		Panel candidatesPanel = new Panel();
		candidatesPanel.setWidth("100%");
		candidatesPanel.setHeight("100%");
		candidatesTable = new Table();
		candidatesTable.addContainerProperty("Name", String.class, null);
		candidatesTable.addContainerProperty("Email", String.class, null);
		candidatesTable.addContainerProperty("Region", String.class, null);
		candidatesTable.addContainerProperty("Age", Integer.class, null);
		candidatesTable.addContainerProperty("AHP weight", Double.class, null);
		candidatesTable.addContainerProperty("URL", String.class, null);
		candidatesTable.setSizeFull();
		candidatesTable.setReadOnly(true);
		candidatesPanel.setContent(candidatesTable);
		
		Panel vacanciesPanel = new Panel();
		vacanciesPanel.setWidth("100%");
		vacanciesPanel.setHeight("100%");
		VerticalLayout vacancies = new VerticalLayout();
		
		logOutButton = new Button("Log Out");
		logOutButton.setStyleName(BaseTheme.BUTTON_LINK);
		logOutButton.setWidth("100%");
		vacancies.addComponent(logOutButton);
		
		for(String vacancyName : presenter.getVacancies()){
			vacancies.addComponent(new Button(vacancyName));
		}
		
		Iterator<Component> it = vacancies.iterator();
		while(it.hasNext()){
			Button vacancyButton = (Button) it.next();
			if(vacancyButton.getCaption().equals("Log Out") || 
					vacancyButton.getCaption().equals("+ New Vacancy")){
				continue;
			}
			vacancyButton.setWidth("100%");
			vacancyButton.addClickListener(new ClickListener(){

				@Override
				public void buttonClick(ClickEvent event) {
					
					presenter.checkForNewCandidates(vacancyButton.getCaption());
					presenter.showCandidates(vacancyButton.getCaption(), candidatesTable);
				}});
		}
		
		newVacancyButton = new Button("+ New Vacancy");
		newVacancyButton.setStyleName(BaseTheme.BUTTON_LINK);
		newVacancyButton.setWidth("100%");
		newVacancyButton.addClickListener(new ClickListener(){

			@Override
			public void buttonClick(ClickEvent event) {
				//
				presenter.createVacancy();
			}});
		vacancies.addComponent(newVacancyButton);
		vacanciesPanel.setContent(vacancies);

		HorizontalLayout main = new HorizontalLayout(vacanciesPanel, candidatesPanel);
		main.setSpacing(true);
		main.setSizeFull();
		main.setExpandRatio(vacanciesPanel, 0.4f);
		main.setExpandRatio(candidatesPanel, 1.6f);
		
		setCompositionRoot(main);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
	}

}
