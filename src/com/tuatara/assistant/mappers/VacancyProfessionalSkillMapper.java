package com.tuatara.assistant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tuatara.assistant.domain.VacancyProfessionalSkill;

public interface VacancyProfessionalSkillMapper {

	 public void insertProfessionalSkill(@Param("id") Integer vacancyId, @Param("ps") VacancyProfessionalSkill vacancyProfSkill);
	 
	 public List<VacancyProfessionalSkill> getProfessionalSkillsByVacancyId(Integer vacancyId);
	 
	 public void deleteProfessionalSkillByVacancyId(Integer vacancyId);

}
