package com.tuatara.assistant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tuatara.assistant.domain.Vacancy;

public interface VacancyMapper {
	
	 public void insertVacancy(@Param("id") Integer userId, @Param("vacancy") Vacancy vacancy);
	 
	 public Vacancy getVacancyById(Integer vacancyId);
	 
	 public Vacancy getVacancyByName(String vacancyName);
	 
	 public List<Vacancy> getVacanciesByUserId(Integer userId);
	 
	 public void updateVacancy(Vacancy vacancy);
	 
	 public void deleteVacancy(Integer vacancyId);
}
