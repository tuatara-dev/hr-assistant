package com.tuatara.assistant.mappers;

import java.util.List;

import com.tuatara.assistant.domain.User;

public interface UserMapper {

	 public void insertUser(User user);
	 
	 public User getUserById(Integer userId);
	 
	 public User getUserByEmail(String email);
	 
	 public List<User> getAllUsers();
	 
	 public void updateUser(User user);
}
