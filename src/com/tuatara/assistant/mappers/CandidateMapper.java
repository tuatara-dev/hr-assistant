package com.tuatara.assistant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tuatara.assistant.domain.Candidate;

public interface CandidateMapper {

	 public void insertCandidate(@Param("id") Integer vacancyId, @Param("candidate") Candidate candidate);
	 
	 public Candidate getCandidateById(Integer candidateId);
	 
	 public Candidate getCandidateByEmail(String email);
	 
	 public List<Candidate> getCandidatesByVacancyId(Integer vacancyId);
	 
	 public List<Candidate> getCandidatesByVacancyName(@Param("vacancyName") String vacancyName);
	 
	 public void deleteCandidateById(Integer candidateId);
}
