package com.tuatara.assistant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tuatara.assistant.domain.LanguageSkill;

public interface LanguageSkillMapper {

	public void insertLanguageSkill(@Param("id") Integer candidateId, @Param("ls") LanguageSkill ls);
	 
	public List<LanguageSkill> getLanguageSkills(Integer candidateId);
	 
	public void deleteLanguageSkills(Integer candidateId);
}
