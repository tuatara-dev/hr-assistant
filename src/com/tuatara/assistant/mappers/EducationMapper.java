package com.tuatara.assistant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tuatara.assistant.domain.Education;

public interface EducationMapper {

	public void insertEducation(@Param("id") Integer candidateId, @Param("education") Education education);
	 
	public List<Education> getEducation(Integer candidateId);
	 
	public void deleteEducation(Integer candidateId);

}
