package com.tuatara.assistant.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tuatara.assistant.domain.Experience;

public interface ExperienceMapper {

	public void insertExperience(@Param("id") Integer candidateId, @Param("experience") Experience experience);
	 
	public List<Experience> getExperience(Integer candidateId);
	 
	public void deleteExperience(Integer candidateId);

}
