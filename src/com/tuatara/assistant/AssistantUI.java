package com.tuatara.assistant;

import javax.servlet.annotation.WebServlet;

import com.tuatara.assistant.service.AuthenticationService;
import com.tuatara.assistant.view.CandidatesView;
import com.tuatara.assistant.view.CreateVacancyView;
import com.tuatara.assistant.view.LoginView;
import com.tuatara.assistant.view.RegisterView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("valo")
public class AssistantUI extends UI {

	private static Navigator navigator;
	
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = AssistantUI.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		
		// Create and add views to navigator
		navigator = new Navigator(this, this);
		
		navigator.addView("login", 		new LoginView());
		navigator.addView("register", 	new RegisterView());
		navigator.addView("newvacancy", new CreateVacancyView());
		
		checkUserSession();
	}
	
	private void checkUserSession() {
		
		if (AuthenticationService.isUserAuthenticated()){
			navigator.addView("candidates", new CandidatesView());
			this.getNavigator().navigateTo(CandidatesView.NAME);
		}
	}
	
	public Navigator getNavigator(){
		return navigator;
	}
}