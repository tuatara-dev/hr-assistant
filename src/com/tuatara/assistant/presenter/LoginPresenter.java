package com.tuatara.assistant.presenter;

import com.tuatara.assistant.dao.UserDAO;
import com.tuatara.assistant.domain.User;
import com.tuatara.assistant.service.AuthenticationService;
import com.tuatara.assistant.view.CandidatesView;
import com.tuatara.assistant.view.LoginView;
import com.tuatara.assistant.view.RegisterView;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

// TODO use MVPLite Framework
public class LoginPresenter {
	
	private LoginView view;

	public LoginPresenter(LoginView view) {
		this.view = view;
	}
	
	public void doLogin(TextField loginField, PasswordField passwordField){
		
		view.enableFields(false);
		
		if (!loginField.isValid() || !passwordField.isValid()) {
			view.enableFields(true);
			Notification.show("Login or Password is't valid. Please try again.", Notification.Type.HUMANIZED_MESSAGE);
	    } else {
		    String login 	= loginField.getValue();
			String password = passwordField.getValue();
			
			User user = new UserDAO().getUserByEmail(login);
			
			if(user != null && AuthenticationService.isPasswordCorrect(user, password)){

				AuthenticationService.setUser(user);
				
				UI.getCurrent().getNavigator().addView("candidates", new CandidatesView());
				UI.getCurrent().getNavigator().navigateTo(CandidatesView.NAME);
				
			} else {
				Notification.show("Login Failed. Please try again.", Notification.Type.HUMANIZED_MESSAGE);
			}
			
			view.enableFields(true);
	    }
	}

	public void goRegister(){
		UI.getCurrent().getNavigator().navigateTo(RegisterView.NAME);
	}
}
