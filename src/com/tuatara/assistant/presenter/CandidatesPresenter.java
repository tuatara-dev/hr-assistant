package com.tuatara.assistant.presenter;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import com.tuatara.assistant.dao.CandidateDAO;
import com.tuatara.assistant.dao.VacancyDAO;
import com.tuatara.assistant.domain.Candidate;
import com.tuatara.assistant.domain.User;
import com.tuatara.assistant.domain.Vacancy;
import com.tuatara.assistant.service.AuthenticationService;
import com.tuatara.assistant.service.ahp.AnalyticHierarchyProcess;
import com.tuatara.assistant.service.email.EmailService;
import com.tuatara.assistant.view.CandidatesView;
import com.tuatara.assistant.view.CreateVacancyView;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;

//TODO use MVPLite Framework
public class CandidatesPresenter {
	
	private User user;
	private CandidatesView view;
	
	public CandidatesPresenter(CandidatesView view) {
		user = AuthenticationService.getUser();
		this.view = view;
	}

	public List<String> getVacancies(){
		List<Vacancy> vacancies = null;
		List<String> vacanciesName = null;
		if(user != null){
			vacancies = new VacancyDAO().getVacanciesByUserId(user.getUserId());
			vacanciesName = new ArrayList<>();
			for(Vacancy vacancy : vacancies){
				vacanciesName.add(vacancy.getVacancyName());
			}
		}
		return vacanciesName;
	}
	
	public void checkForNewCandidates(String vacancyName){
		Vacancy vacancy = new VacancyDAO().getVacancyByName(vacancyName);
		CandidateDAO candidateDAO = new CandidateDAO();
		EmailService emailService = new EmailService(user, vacancy);
		List<Candidate> newCAndidates = null;
		try {
			newCAndidates = emailService.readNewMessages();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		if(newCAndidates != null){
			for(Candidate candidate : newCAndidates){
				candidateDAO.insertCandidate(vacancy.getVacancyId(), candidate);
			}
		}
	}
	
	public void showCandidates(String vacancyName, Table candidatesTable){
		
		candidatesTable.removeAllItems();
		
		Vacancy vacancy = new VacancyDAO().getVacancyByName(vacancyName);
		List<Candidate> candidates = new CandidateDAO().getCandidatesByVacancyName(vacancyName);
		
		AnalyticHierarchyProcess ahp = new AnalyticHierarchyProcess(vacancy, candidates);
		double[] weights = ahp.getGlobalWeights();
		
		int i=0;
		for(Candidate candidate : candidates){
			candidatesTable.addItem(new Object[]{
					candidate.getCandidateName(),
					candidate.getCandidateEmail(),
					candidate.getRegion(),
					candidate.getAge(),
					weights[i],
					candidate.getResumeURL()}, i);
			i++;
		}
	}
	
	public void createVacancy(){
		UI.getCurrent().getNavigator().navigateTo(CreateVacancyView.NAME);
	}
	
}
