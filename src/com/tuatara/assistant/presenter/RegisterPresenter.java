package com.tuatara.assistant.presenter;

import com.tuatara.assistant.dao.UserDAO;
import com.tuatara.assistant.domain.User;
import com.tuatara.assistant.service.AuthenticationService;
import com.tuatara.assistant.view.CandidatesView;
import com.tuatara.assistant.view.RegisterView;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

//TODO use MVPLite Framework
public class RegisterPresenter {

	private RegisterView view;
	
	public RegisterPresenter(RegisterView view) {
		this.view = view;
	}
	
	public void doRegister(){
		
		if(!checkFieldsValidation()){
			Notification.show("Input values isn't valid", Notification.Type.HUMANIZED_MESSAGE);
			return;
		} else {
			String userName = view.getNameField().getValue();
			String login = view.getLoginField().getValue();
			String password = view.getPasswordField().getValue();
			String imapHost = view.getImapHostField().getValue();
			int imapPort = Integer.parseInt(view.getImapPortField().getValue());
			String smtpHost = view.getSmtpHostField().getValue();
			int smtpPort = Integer.parseInt(view.getSmtpPortField().getValue());
			
			// create user
			User user = new User(userName, login, password, imapHost, imapPort, smtpHost, smtpPort);
			// insert to database
			new UserDAO().insertUser(user);
			// set user session
			AuthenticationService.setUser(user);
			// go to candidates view
			UI.getCurrent().getNavigator().addView("candidates", new CandidatesView());
			UI.getCurrent().getNavigator().navigateTo(CandidatesView.NAME);
		}
	}
	
	private boolean checkFieldsValidation(){
		
		String pass = view.getPasswordField().getValue();
		String repass = view.getRepeatedField().getValue();
		if(!pass.equals(repass)){
			return false;
		}
		
		for(AbstractTextField tf : view.getFields()){
			if(!tf.isValid()){
				return false;
			}
		}
		return true;
	}

}
