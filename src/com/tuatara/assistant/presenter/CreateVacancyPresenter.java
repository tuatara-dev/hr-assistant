package com.tuatara.assistant.presenter;

import java.util.ArrayList;
import java.util.List;

import com.tuatara.assistant.dao.VacancyDAO;
import com.tuatara.assistant.domain.Vacancy;
import com.tuatara.assistant.domain.VacancyProfessionalSkill;
import com.tuatara.assistant.service.AuthenticationService;
import com.tuatara.assistant.view.CandidatesView;
import com.tuatara.assistant.view.CreateVacancyView;
import com.vaadin.ui.UI;

//TODO use MVPLite Framework
public class CreateVacancyPresenter {

	CreateVacancyView view;
	
	public CreateVacancyPresenter(CreateVacancyView view) {
		this.view = view;
	}
	
	public void createVacancy(){
		
		// get values from components
		String vacancyName = view.getNameField().getValue();
		String autoResponse = view.getAutoResponseArea().getValue();
		String region = view.getRegionField().getValue();
		List<VacancyProfessionalSkill> profSkills = new ArrayList<>();
		for(Object itemId : view.getProfessionalSkillsComboBox().getItemIds()){
			profSkills.add(new VacancyProfessionalSkill(view.getProfessionalSkillsComboBox().getItem(itemId).toString()));
		}
		String profSkillsPriority = (String) view.getProfessionalSkillsPriority().getValue();
		// not used
		Integer experience = view.getExperienceSlider().getValue().intValue();
		String expPriority = (String) view.getExperiencePriority().getValue();
		String education = null;
		String eduPriority = (String) view.getEducationPriority().getValue();
		// not used
		String training = null;
		// not used
		String trnPriority = (String) view.getCoursesPriority().getValue();
		String language = (String) view.getLanguageSkillsList().getValue();
		String lanPriority = (String) view.getLanguageSkillsPriority().getValue();
		
		// create new vacancy
		Vacancy vacancy = new Vacancy(vacancyName, autoResponse, region, profSkills, language, lanPriority,
				education, eduPriority, expPriority, profSkillsPriority, null);
		
		// check user session);
		if (!AuthenticationService.isUserAuthenticated()){
			return;
		} else {
			// insert new vacancy to DB
			new VacancyDAO().insertVacancy(AuthenticationService.getUser().getUserId(), vacancy);
		}

		// return to CandidatesView
		UI.getCurrent().getNavigator().addView("candidates", new CandidatesView());
		UI.getCurrent().getNavigator().navigateTo(CandidatesView.NAME);
	}
	
	public void doCancel(){
		if (AuthenticationService.isUserAuthenticated()){
			UI.getCurrent().getNavigator().navigateTo(CandidatesView.NAME);
		}
	}

}
