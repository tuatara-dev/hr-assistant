package com.tuatara.assistant.service.email;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;

import com.tuatara.assistant.dao.CandidateDAO;
import com.tuatara.assistant.domain.Education;
import com.tuatara.assistant.domain.Experience;
import com.tuatara.assistant.domain.LanguageSkill;
import com.tuatara.assistant.domain.User;
import com.tuatara.assistant.domain.Vacancy;
import com.tuatara.assistant.domain.Candidate;
import com.tuatara.assistant.service.cv.ItRabotaUaParser;

public class EmailService {

	private User user;
	private Vacancy vacancy;
	
	public EmailService(User user, Vacancy vacancy) {
		this.user = user;
		this.vacancy = vacancy;
	}

	private Session imapAuthentication(User user){
		Properties props = new Properties();
		props.put("mail.host", user.getImapHost());
		props.put("mail.port", user.getImapPort());
		props.put("mail.transport.protocol", "imaps");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user.getEmail(), user.getPassword());
                    }
                });
        return session;
	}
	/*
	private Session smtpAuthentication(User user){
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", user.getSmtpHost());
		props.put("mail.smtp.port", user.getSmtpPort());
		Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user.getEmail(), user.getPassword());
                    }
                });
		return session;
	}
	*/
	public List<Candidate> readNewMessages() throws MessagingException{
		
		List<Candidate> newCandidates = new ArrayList<>();
		
		//Session smtpSession = smtpAuthentication(user);
		
		Store store = imapAuthentication(user).getStore("imaps");
        store.connect();
        
        Folder inbox = store.getFolder("INBOX");
        Folder vacancyFolder = store.getFolder(vacancy.getVacancyName());
        
        if (inbox == null || !inbox.exists()) {
			// Inbox folder empty or not exist
			return null;
		} else {
			inbox.open(Folder.READ_WRITE);
			try{
				if (!vacancyFolder.exists()) {
					vacancyFolder.create(Folder.HOLDS_MESSAGES);
				}
				vacancyFolder.open(Folder.READ_WRITE);
				try{
					Message messages[] = inbox.search(new FlagTerm(
			                new Flags(Flag.SEEN), false));
					for(Message message : messages){
						
						if(vacancy.getVacancyName().equals(message.getSubject())){
							String content = handleMessage(message);
							
							if(content != null){
								String email = message.getFrom()[0].toString();
								newCandidates.add(getCandidateFromCV(content, email));
								//sendMessage(smtpSession, email);
								moveMessage(message, inbox, vacancyFolder);
							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally{
					vacancyFolder.close(true);
				}
			} finally{
				inbox.close(true);
			}
		}
        store.close();
        
        return newCandidates;
	}
	
	private String handleMessage(Message message) throws IOException{
		try {
			Address[] from = message.getFrom();
			if(new CandidateDAO().getCandidateByEmail(from[0].toString()) == null){
				// this is new candidate
				// content == null
				if (message.getContent() instanceof String){
					
					return (String) message.getContent();
				} else {
					// we don't handle messages with Multipart content
				}
				
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private Candidate getCandidateFromCV(String url, String email) throws IOException{
		
		ItRabotaUaParser parser = new ItRabotaUaParser(url);
		
		String name = parser.getName();
		int age = parser.getAge();
		String phone = parser.getPhone();
		// not used yet
		//String email = parser.getEmail();
		String region = parser.getRegion();
		String profSkills = parser.getProfessionalSkills();
		List<Education> education= parser.getEducation();
		List<LanguageSkill> languageSkills = parser.getLanguageSkills();
		List<Experience> experience = parser.getExperience();
		// not used yet
		//List<String> trainings = parser.getTraining();
		
		return new Candidate(name, email, phone, url, region, age, profSkills, 
				languageSkills, experience, education);
	}
	
	private void moveMessage(Message message, Folder from, Folder to) throws MessagingException{
		from.copyMessages(new Message[] {message}, to);
		message.setFlag(Flags.Flag.DELETED, true);
	}
	/*
	private void sendMessage(Session session, String recipient) throws MessagingException{
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(user.getEmail()));
		message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(recipient));
		message.setSubject(vacancy.getVacancyName());
		message.setText(vacancy.getAutoresponceMessage());
		Transport.send(message);
	}
	*/
}
