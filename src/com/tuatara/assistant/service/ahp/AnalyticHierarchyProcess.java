package com.tuatara.assistant.service.ahp;
	
import java.util.List;
import java.util.stream.DoubleStream;
import com.tuatara.assistant.domain.Candidate;
import com.tuatara.assistant.domain.LanguageSkill;
import com.tuatara.assistant.domain.Vacancy;
import com.tuatara.assistant.domain.VacancyProfessionalSkill;
	
	public class AnalyticHierarchyProcess {
		
		Vacancy vacancy;
		List<Candidate> candidates;
		
		public AnalyticHierarchyProcess(Vacancy vacancy, List<Candidate> candidates) {
			this.vacancy = vacancy;
			this.candidates = candidates;
		}
		
		private double[] calculateCriteriasWeights(){
			
			// convert vacancy criterias
			double[] criterias 	 = {
					convertPriority(vacancy.getEducationPriority()),
					convertPriority(vacancy.getExperiencePriority()),
					convertPriority(vacancy.getProfessionalSkillsPriority()),
					convertPriority(vacancy.getLanguagePriority()),
			};
			
			return calculateWeights(criterias);
		}
	
		
		private double[][] calculateLocalWeights(){
			
			// Create priorities array for every criteria
			double[] education 	= new double[candidates.size()];
			double[] experience = new double[candidates.size()];
			double[] profSkills = new double[candidates.size()];
			double[] language 	= new double[candidates.size()];
			
			List<VacancyProfessionalSkill> vps = vacancy.getProfesionalSkills();
			
			for(int i=0; i<candidates.size(); i++){
				
				Candidate candidate = candidates.get(i);
				
				education[i] = candidate.getEducation() == null? 0 : candidate.getEducation().size();
				experience[i] = candidate.getExperience()== null? 0 : candidate.getExperience().size();
				
				if(candidate.getProfessionalSkills() != null){
					int pro = 0;
					for(VacancyProfessionalSkill ps : vps){
						if(candidate.getProfessionalSkills().contains(ps.getSkill())){
							pro++;
						}
					}
					profSkills[i] = pro;
				}
				
				profSkills[i] = (profSkills[i] == 0) ? 1 : profSkills[i];
				
				List<LanguageSkill> lanSkills = candidate.getLanguageSkills();
				if(lanSkills != null){
					for(LanguageSkill ls : lanSkills){
						if (ls.getLanguage().equals(vacancy.getLanguage())){
							language[i] = convertLangLevel(ls.getSkill());
						}
					}
				}
				language[i] = (language[i] == 0) ? 1 : language[i];
			}
			
			// find maximal value for normalization
			double maxEdu = DoubleStream.of(education).max().orElse(1);
			double maxExp = DoubleStream.of(experience).max().orElse(1);
			double maxPro = DoubleStream.of(profSkills).max().orElse(1);
			
			// normalize criteria priority (1 - minimal priority, 9 - maximal priority)
			for(int i=0; i<candidates.size(); i++){
				education[i] = Math.round((education[i]*9)/maxEdu);
				education[i] = (education[i] < 1) ? 1 : education[i];
				experience[i] = Math.round((experience[i]*9)/maxExp);
				experience[i] = (experience[i] < 1) ? 1 : experience[i];
				profSkills[i] = Math.round((profSkills[i]*9)/maxPro);
				profSkills[i] = (profSkills[i] < 1) ? 1 : profSkills[i];
			}
			
			//calculates candidates weights for every criteria  
			double[][] lPriorities = new double[][]{
					calculateWeights(education),
					calculateWeights(experience),
					calculateWeights(profSkills),
					calculateWeights(language)
			};
			
			return lPriorities;
		}
		
		private double[] calculateWeights(double[] criteries){
			double[] weights = new double[criteries.length];
			double[][] MatrixOfPairwiseComparisons = new double[criteries.length][criteries.length];
			// temporary row for weights calculation
			double[] tmp = new double[criteries.length];
			
			for(int i=0; i<criteries.length; i++){
				for(int j=i; j<criteries.length; j++){
					if(i == j){
						MatrixOfPairwiseComparisons[i][j] = 1;
					} else {
						MatrixOfPairwiseComparisons[i][j] = criteries[i]/criteries[j];
						MatrixOfPairwiseComparisons[j][i] = 1/MatrixOfPairwiseComparisons[i][j];
					}
				}
				tmp[i] = normalize(MatrixOfPairwiseComparisons[i]);
			}
			
			// calculate weights
			double sum = DoubleStream.of(tmp).sum();
			for(int i=0; i<criteries.length; i++){
				weights[i] = tmp[i] / sum;
			}
			
			return weights;
		}
		
		private double normalize(double[] row){
		
			double mult = 1;
			for(double el : row){
				mult *= el;
			}
			return Math.pow(mult, 1.0/row.length);
		}
		
		private double convertPriority(String value){
			switch(value){
				case "Not important": 	return 1;
				case "Low": 			return 3;
				case "Middle": 			return 6;
				case "High": 			return 9;
				default: 				return 1;
			}
		}
		
		private double convertLangLevel(String value){
			switch(value){
				case "elementary": 			return 2;
				case "lower intermediate": 	return 3;
				case "intermediate": 		return 4;
				case "upper intermediate": 	return 5;
				case "advanced": 			return 6;
				case "fluent": 				return 7;
				case "native": 				return 8;
				default: 					return 1;
			}
		}
		
		public double[] getGlobalWeights(){
			
			double[] cWeights = calculateCriteriasWeights();
			double[][] lWeights = calculateLocalWeights();
			double[] gWeights = new double[candidates.size()];
			// temporary array for calculation global weights
			double[] candTmpIndex = new double[candidates.size()];
			
			// normalization
			for(int i=0; i<candidates.size(); i++){
				double value = (lWeights[0][i] + cWeights[0]) * 
							(lWeights[1][i] + cWeights[1]) * 
							(lWeights[2][i] + cWeights[2]) * 
							(lWeights[3][i] + cWeights[3]);
				candTmpIndex[i] = Math.pow(value, 1.0/4);
			}
			
			double maxW = DoubleStream.of(candTmpIndex).sum();
			for(int i=0; i<candidates.size(); i++){
				gWeights[i] = candTmpIndex[i] / maxW;
			}
			
			return gWeights;
		}
	}
