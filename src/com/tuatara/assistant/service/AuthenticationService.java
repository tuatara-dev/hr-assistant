package com.tuatara.assistant.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.tuatara.assistant.domain.User;
import com.tuatara.assistant.view.LoginView;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.UI;

public class AuthenticationService {

	public static void setUser(User user){
		
		UI.getCurrent().getSession().setAttribute("user", user);
		VaadinService.getCurrentRequest().getWrappedSession()
        .setAttribute("user", user);
	}
	
	public static User getUser(){
		
		return (User) UI.getCurrent().getSession().getAttribute("user");
	}
	
	public static void removeUser(){
		setUser(null);
	}
	
	public static boolean isUserAuthenticated(){
		if (UI.getCurrent().getSession().getAttribute("user") == null){
			UI.getCurrent().getNavigator().navigateTo(LoginView.NAME);
			return false;
		} else {
			return true;
		}
	}
	
	public static boolean isPasswordCorrect(User user, String password){
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.matches(password, passwordEncoder.encode(user.getPassword()));
	}

}
