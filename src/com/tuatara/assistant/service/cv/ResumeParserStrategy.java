package com.tuatara.assistant.service.cv;

import java.util.List;

import com.tuatara.assistant.domain.Education;
import com.tuatara.assistant.domain.Experience;
import com.tuatara.assistant.domain.LanguageSkill;

public interface ResumeParserStrategy {
	
	String getName();
	
	int getAge();
	
	String getPhone();
	
	String getEmail();
	
	String getRegion();
	
	String getProfessionalSkills();
	
	List<Education> getEducation();
	
	List<Experience> getExperience();
	
	List<String> getTraining();
	
	List<LanguageSkill> getLanguageSkills();
}
