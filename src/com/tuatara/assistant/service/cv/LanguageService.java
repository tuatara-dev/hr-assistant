package com.tuatara.assistant.service.cv;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LanguageService {

	private static Map<String, List<String>> Languages = new HashMap<>();
	private static Map<String, List<String>> Levels = new HashMap<>(); 
	
	public LanguageService(){
		Languages.put("English", Arrays.asList( new String[] {"English", "����������", "���������"}));
		Languages.put("Spanish", Arrays.asList( new String[] {"Spanish", "���������", "���������"}));
		Languages.put("Italian", Arrays.asList( new String[] {"Italian", "�����������", "���������"}));
		Languages.put("German", Arrays.asList( new String[] {"German", "��������", "ͳ������"}));
		Languages.put("French", Arrays.asList( new String[] {"French", "�����������", "����������"}));
		Languages.put("Russian", Arrays.asList( new String[] {"Russian", "�������", "��������"}));
		Languages.put("Ukrainian", Arrays.asList( new String[] {"Ukrainian", "����������", "���������"}));
		
		Levels.put("elementary", Arrays.asList( new String[] {"elementary", "�������", "�������"}));
		Levels.put("lower intermediate", Arrays.asList( new String[] {"lower intermediate", "���� ��������", "����� ����������"}));
		Levels.put("intermediate", Arrays.asList( new String[] {"intermediate", "�������", "��������"}));
		Levels.put("upper intermediate", Arrays.asList( new String[] {"upper intermediate", "���� ��������", "���� ����������"}));
		Levels.put("advanced", Arrays.asList( new String[] {"advanced", "�����������", "�����������"}));
		Levels.put("fluent", Arrays.asList( new String[] {"fluent", "��������", "�����"}));
		Levels.put("native", Arrays.asList( new String[] {"native", "������", "����"}));
	}
	
	public String getLanguage(String language){
		for(Map.Entry<String, List<String>> entry : Languages.entrySet()){
			if(entry.getValue().contains(language)){
				return entry.getKey();
			}
		}
		return language;
	}
	
	public String getLevel(String level){
		for(Map.Entry<String, List<String>> entry : Levels.entrySet()){
			if(entry.getValue().contains(level)){
				return entry.getKey();
			}
		}
		return level;
	} 
}
