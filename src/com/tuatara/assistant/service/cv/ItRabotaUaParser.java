package com.tuatara.assistant.service.cv;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.tuatara.assistant.domain.Education;
import com.tuatara.assistant.domain.Experience;
import com.tuatara.assistant.domain.LanguageSkill;

public class ItRabotaUaParser implements ResumeParserStrategy {

	private final Document doc;
	
	public ItRabotaUaParser(String cv) throws IOException{
		
		doc = Jsoup.connect(cv).get();
	}
	
	@Override
	public String getName() {
		
		return doc.getElementById("centerZone_BriefResume1_CvView1_cvHeader_lblName").text();
	}

	@Override
	public int getAge() {
		
		// Get first Element for class rua-p-t_12
		Element user = doc.getElementsByClass("rua-p-t_12").first();
		// Get separator between region and age
		String separator = doc.getElementById("centerZone_BriefResume1_CvView1_cvHeader_ltCity_ltAge_Separator").text();
		// Get the first two symbols
		String age = user.text().split(separator)[1].substring(0, 2);
		
		return Integer.parseInt(age);
	}

	@Override
	public String getPhone() {
		// TODO Contacs avaible only for registered HR users
		return null;
	}

	@Override
	public String getEmail() {
		// TODO Contacs avaible only for registered HR users
		return null;
	}

	@Override
	public String getRegion() {
		
		// Get first Element for class rua-p-t_12
		Element user = doc.getElementsByClass("rua-p-t_12").first();
		// Get separator between region and age
		String separator = doc.getElementById("centerZone_BriefResume1_CvView1_cvHeader_ltCity_ltAge_Separator").text();
		// Return region
		return user.text().split(separator)[0];
	}

	@Override
	public String getProfessionalSkills() {
		
		return doc.getElementById("centerZone_BriefResume1_CvView1_Skills_txtSkills").text();
	}

	@Override
	public List<LanguageSkill> getLanguageSkills() {
		
		List<LanguageSkill> languageSkills = new ArrayList<>();
		Element languages = doc.getElementById("LanguagesHolder");
		String separator = " - ";
		
		LanguageService lanConverter = new LanguageService();
		for(Element elt : languages.getElementsByTag("p")){
			String language = lanConverter.getLanguage(elt.text().split(separator)[0]);
			String level 	= lanConverter.getLevel(elt.text().split(separator)[1]);
			languageSkills.add(new LanguageSkill(language, level));
		}
		
		return languageSkills;
	}

	@Override
	public List<Education> getEducation() {
		
		List<Education> education = new ArrayList<>();
		
		List<String> universityName = new ArrayList<>();
		List<Integer> endDate = new ArrayList<>();
		List<String> faculty = new ArrayList<>();
		List<String> degree = new ArrayList<>();
		
		Element elt = doc.getElementById("EducationHolder");

		String separator = ",&nbsp;";
		
		for(Node node : elt.childNodes()){

			switch(node.nodeName()){
				case "h2":
				case "hr":
					continue;
				case "p": {
					for(Node nodes : node.childNodes()){
						switch(nodes.nodeName()){
							case "b":{
								universityName.add(removeTag(nodes.toString()));
								break;
							}
							case "span":{
								endDate.add(Integer.parseInt(removeTag(nodes.toString()).split(" ")[2]));
								break;
							}
						}
					}
					break;
				}
				case "#text": {
					faculty.add(node.toString().split(separator)[0]);
					degree.add(node.toString().split(separator)[1]);
					break;
				}
			}
		}
		
		for(int i=0; i<universityName.size(); i++){
			education.add(new Education(
					universityName.get(i),
					endDate.get(i),
					faculty.get(i),
					degree.get(i)));
		}
		
		return education;
	}

	private static String removeTag(String data){
		int left = data.indexOf(">")+1;
		int right = data.lastIndexOf("<");
		return data.substring(left, right);
	}
	
	@Override
	public List<Experience> getExperience() {
		
		List<Experience> experience = new ArrayList<>();
		Element exp = doc.getElementById("ExperienceHolder");
		Elements elts = exp.getAllElements();

		
		String position	 	= null;
		String company	 	= null;
		String description 	= null;
		String period 		= null;

		for(Element element:elts){
			switch(element.tagName()){
				case "p":{
					for(Element subElement:element.getAllElements()){
						switch(subElement.tagName()){
							case "b":{
								if("muted".equals(element.className())){
									company = subElement.text();
								} else {
									position = subElement.text();
								}
								break;
							}
							case "em":{
								if("muted".equals(element.className())){
									description = subElement.text();
								} else {
									period = subElement.text();
								}
								break;
							}
						}
					}
					break;
				}
				case "hr":{
					experience.add(new Experience(position, company, description, period));
					break;
				}
			}
		}
		experience.add(new Experience(position, company, description, period));
		return experience;
	}

	@Override
	public List<String> getTraining() {
		
		List<String> trainings = new ArrayList<>();
		Element exp = doc.getElementById("TrainingsHolder");
		Elements elts = exp.getElementsByTag("b");
		
		for(Element e:elts){			
			trainings.add(e.text());
		}
		
		return trainings;
	}
}
