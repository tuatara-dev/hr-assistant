package com.tuatara.assistant.domain;

public class LanguageSkill {

	private String language;
	private String skill;
	
	public LanguageSkill(){
		
	}
	
	public LanguageSkill(String language, String skill) {
		this.language = language;
		this.skill = skill;
	}

	public String getLanguage() {
		return language;
	}

	public String getSkill() {
		return skill;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

}
