package com.tuatara.assistant.domain;

import java.util.List;

public class Candidate {
	
	private Integer 			candidateId;
	private String 				candidateName;
	private String 				candidateEmail;
	private String 				phone;
	private String				resumeURL;
	private String 				region;
	private Integer 			age;
	private String		 		professionalSkills;
	private List<LanguageSkill> languageSkills;
	private List<Experience> 	experience;
	private List<Education> 	education;
	
	public Candidate(){
		
	}

	public Candidate(String candidateName, String candidateEmail, String phone,
			String resumeURL, String region, Integer age,
			String professionalSkills, List<LanguageSkill> languageSkills,
			List<Experience> experience, List<Education> education) {

		this.candidateName = candidateName;
		this.candidateEmail = candidateEmail;
		this.phone = phone;
		this.resumeURL = resumeURL;
		this.region = region;
		this.age = age;
		this.professionalSkills = professionalSkills;
		this.languageSkills = languageSkills;
		this.experience = experience;
		this.education = education;
	}

	public Integer getCandidateId() {
		return candidateId;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public String getCandidateEmail() {
		return candidateEmail;
	}

	public String getPhone() {
		return phone;
	}

	public String getResumeURL() {
		return resumeURL;
	}

	public String getRegion() {
		return region;
	}

	public Integer getAge() {
		return age;
	}

	public String getProfessionalSkills() {
		return professionalSkills;
	}

	public List<LanguageSkill> getLanguageSkills() {
		return languageSkills;
	}

	public List<Experience> getExperience() {
		return experience;
	}

	public List<Education> getEducation() {
		return education;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public void setCandidateEmail(String candidateEmail) {
		this.candidateEmail = candidateEmail;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setResumeURL(String resumeURL) {
		this.resumeURL = resumeURL;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setProfessionalSkills(String professionalSkills) {
		this.professionalSkills = professionalSkills;
	}

	public void setLanguageSkills(List<LanguageSkill> languageSkills) {
		this.languageSkills = languageSkills;
	}

	public void setExperience(List<Experience> experience) {
		this.experience = experience;
	}

	public void setEducation(List<Education> education) {
		this.education = education;
	}
	
}
