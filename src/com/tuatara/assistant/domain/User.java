package com.tuatara.assistant.domain;

import java.util.List;

public class User {
	
	private Integer userId;
	private String userName;
	private String email;
	private String password;
	private String imapHost;
	private int imapPort;
	private String smtpHost;
	private int smtpPort;
	private List<Vacancy> vacancies;
	
	public User(){
		
	}
	
	public User(String userName, String email, String password,
			String imapHost, int imapPort, String smtpHost, int smtpPort) {

		this.userName = userName;
		this.email = email;
		this.password = password;
		this.imapHost = imapHost;
		this.imapPort = imapPort;
		this.smtpHost = smtpHost;
		this.smtpPort = smtpPort;
	}
	
	public Integer getUserId() {
		return userId;
	}
	public String getUserName() {
		return userName;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	public String getImapHost() {
		return imapHost;
	}
	public int getImapPort() {
		return imapPort;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public int getSmtpPort() {
		return smtpPort;
	}
	public List<Vacancy> getVacancies() {
		return vacancies;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setImapHost(String imapHost) {
		this.imapHost = imapHost;
	}
	public void setImapPort(int imapPort) {
		this.imapPort = imapPort;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}
	public void setVacancies(List<Vacancy> vacancies) {
		this.vacancies = vacancies;
	}

}
