package com.tuatara.assistant.domain;

import java.util.List;

public class Vacancy {
	
	private Integer 						vacancyId;
	private String 							vacancyName;
	private String							autoresponceMessage;
	private String 							region;
	private List<VacancyProfessionalSkill>	profesionalSkills;
	private String 							language;
	private String							languagePriority;
	private String							education;
	private String							educationPriority;
	private String 							experiencePriority;
	private String 							professionalSkillsPriority;
	private List<Candidate>					candidates;
	
	public Vacancy(){
		
	}

	public Vacancy(String vacancyName, String autoresponceMessage,
			String region, List<VacancyProfessionalSkill> profesionalSkills,
			String language, String languagePriority, String education,
			String educationPriority, String experiencePriority,
			String professionalSkillsPriority, List<Candidate> candidates) {

		this.vacancyName = vacancyName;
		this.autoresponceMessage = autoresponceMessage;
		this.region = region;
		this.profesionalSkills = profesionalSkills;
		this.language = language;
		this.languagePriority = languagePriority;
		this.education = education;
		this.educationPriority = educationPriority;
		this.experiencePriority = experiencePriority;
		this.professionalSkillsPriority = professionalSkillsPriority;
		this.candidates = candidates;
	}

	public Integer getVacancyId() {
		return vacancyId;
	}

	public String getVacancyName() {
		return vacancyName;
	}

	public String getAutoresponceMessage() {
		return autoresponceMessage;
	}

	public String getRegion() {
		return region;
	}

	public List<VacancyProfessionalSkill> getProfesionalSkills() {
		return profesionalSkills;
	}

	public String getLanguage() {
		return language;
	}

	public String getLanguagePriority() {
		return languagePriority;
	}

	public String getEducation() {
		return education;
	}

	public String getEducationPriority() {
		return educationPriority;
	}

	public String getExperiencePriority() {
		return experiencePriority;
	}

	public String getProfessionalSkillsPriority() {
		return professionalSkillsPriority;
	}

	public List<Candidate> getCandidates() {
		return candidates;
	}

	public void setVacancyId(Integer vacancyId) {
		this.vacancyId = vacancyId;
	}

	public void setVacancyName(String vacancyName) {
		this.vacancyName = vacancyName;
	}

	public void setAutoresponceMessage(String autoresponceMessage) {
		this.autoresponceMessage = autoresponceMessage;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setProfesionalSkills(
			List<VacancyProfessionalSkill> profesionalSkills) {
		this.profesionalSkills = profesionalSkills;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setLanguagePriority(String languagePriority) {
		this.languagePriority = languagePriority;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public void setEducationPriority(String educationPriority) {
		this.educationPriority = educationPriority;
	}

	public void setExperiencePriority(String experiencePriority) {
		this.experiencePriority = experiencePriority;
	}

	public void setProfessionalSkillsPriority(String professionalSkillsPriority) {
		this.professionalSkillsPriority = professionalSkillsPriority;
	}

	public void setCandidates(List<Candidate> candidates) {
		this.candidates = candidates;
	}

}