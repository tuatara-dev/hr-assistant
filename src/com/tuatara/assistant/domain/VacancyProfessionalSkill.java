package com.tuatara.assistant.domain;

public class VacancyProfessionalSkill {

	Integer vacancyId;
	String skill;

	public VacancyProfessionalSkill(){
		
	}

	public VacancyProfessionalSkill(String skill) {
		this.skill = skill;
	}

	public Integer getVacancyId() {
		return vacancyId;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}
	
}
