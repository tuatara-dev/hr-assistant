package com.tuatara.assistant.domain;

public class Experience {

	private String position;
	private String company;
	private String description;
	private String period;
	
	public Experience(){
		
	}

	public Experience(String position, String company, String description,
			String period) {
		this.position = position;
		this.company = company;
		this.description = description;
		this.period = period;
	}

	public String getPosition() {
		return position;
	}

	public String getCompany() {
		return company;
	}

	public String getDescription() {
		return description;
	}

	public String getPeriod() {
		return period;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

}
