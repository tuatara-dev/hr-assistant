package com.tuatara.assistant.domain;

public class Education {
	
	private String university;
	private String faculty;
	private String degree;
	private Integer endYear;
	
	public Education(){}
	
	public Education(String university, int endYear, String faculty,
			String degree) {
		
		this.university = university;
		this.endYear = endYear;
		this.faculty = faculty;
		this.degree = degree;
	}

	public String getUniversity() {
		return university;
	}

	public String getFaculty() {
		return faculty;
	}

	public String getDegree() {
		return degree;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

}
