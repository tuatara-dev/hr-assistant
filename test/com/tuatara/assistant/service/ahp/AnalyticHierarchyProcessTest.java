package com.tuatara.assistant.service.ahp;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

import org.junit.BeforeClass;
import org.junit.Test;

import com.tuatara.assistant.domain.Candidate;
import com.tuatara.assistant.domain.Vacancy;
import com.tuatara.assistant.domain.VacancyProfessionalSkill;

public class AnalyticHierarchyProcessTest {

	static Vacancy vacancyTest;
	static List<Candidate> candidatesTest;
	AnalyticHierarchyProcess ahp;

	@BeforeClass
    public static void init() {
		
		// Create vacancy with some criterias
		vacancyTest = new Vacancy();
		vacancyTest.setEducationPriority("Low");
		vacancyTest.setExperiencePriority("Hign");
		vacancyTest.setLanguage("English");
		vacancyTest.setLanguagePriority("Low");
		List<VacancyProfessionalSkill> profesionalSkills = new ArrayList<>();
		profesionalSkills.add(new VacancyProfessionalSkill("Java"));
		profesionalSkills.add(new VacancyProfessionalSkill("Spring"));
		profesionalSkills.add(new VacancyProfessionalSkill("Hibernate"));
		profesionalSkills.add(new VacancyProfessionalSkill("SVN"));
		profesionalSkills.add(new VacancyProfessionalSkill("MySQL"));
		profesionalSkills.add(new VacancyProfessionalSkill("MyBatis"));
		profesionalSkills.add(new VacancyProfessionalSkill("Erlang"));
		vacancyTest.setProfesionalSkills(profesionalSkills );
		vacancyTest.setProfessionalSkillsPriority("Hign");
		
		// Create smart candidate
		Candidate candidate1 = new Candidate();
		candidate1.setEducation(new ArrayList<>(2));
		candidate1.setExperience(new ArrayList<>(5));
		candidate1.setProfessionalSkills("Java, Spring, Hibernate, SVN, MySQL, MyBatis, Erlang");
		// Create another candidate
		Candidate candidate2 = new Candidate();
		candidate2.setEducation(new ArrayList<>(2));
		candidate2.setExperience(new ArrayList<>(2));
		candidate2.setProfessionalSkills("Java, Spring, Hibernate, Perforce");
		// Create candidate without experience
		Candidate candidate3 = new Candidate();
		candidate3.setEducation(new ArrayList<>(10));
		candidate3.setProfessionalSkills("Java");
		candidatesTest = new ArrayList<>();
		candidatesTest.add(candidate1);
		candidatesTest.add(candidate2);
		candidatesTest.add(candidate3);
    }
	
	@Test
	public void calculateGlobalWeightsTest(){
		
		ahp = new AnalyticHierarchyProcess(vacancyTest, candidatesTest);
		
		double[] weights = ahp.getGlobalWeights();
		
		assertTrue(weights[0] > weights[1]);
		assertTrue(weights[1] > weights[2]);
		assertTrue(0.01 > 1 - DoubleStream.of(weights).sum());
	}
	
}
